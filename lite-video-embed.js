/*
* This Library ehence progressively <a data-video-embed > elements. 
* Once clicked, linked video is embded.
* No Tiers JS loaded otherwise.
* 
* Inspiration : 
* https://gomakethings.com/how-to-lazy-load-youtube-videos-with-vanilla-javascript/
* https://github.com/paulirish/lite-youtube-embed (But accessible without JS)
*/

lve_autoplayDisable = false
lve_abort = false



/**
 * Handle click events on the video thumbnails
 * @param  {el}         link as a container
 * @param  {callback }  callback function
 */
async function replaceByIframe(el, callback, target) {

  let player,
      id         = el.getAttribute('data-id'),
      platform   = el.getAttribute('data-platform'),
      controls   = el.getAttribute('data-controls'),
      trgt       = platform == "youtube" ?  
                  el.querySelector(target ? `.${target}` : 'button' )
                  : el


  /**
   * Handle click events on the video thumbnails
   * @param  {src}        url to load
  */
  function loadApi(src){
    var script = document.createElement('script');
    script.src = src;
    document.head.appendChild(script);
  }


  /**
   * When falling to serve video, to link
   */
 
  const onState = () => ({

    onPlayerReady: () => {
      console.log("Player is ready")
      addClasses().OnInit()
      if(callback){callback(player)}
    },

    onFailure: () => {
      let url
      if(platform == "vimeo"){
        url = "https://vimeo.com/" + el.dataset.id
      } else if(platform == "youtube"){
        url = "https://www.youtube.com/watch?v=" + el.dataset.id
      } else {
        url = el.getAttribute('data-href')
      }
      window.location.replace(url)
    }
  })

  /**
   * Add Classes based on video states
   */ 
  const addClasses = () => ({
    OnInit: () => {
      console.log("add Classes On Init")
      el.classList.add('playable', 'activated');
    },

    OnPlayState: () => {
      console.log("add Classes On Play State")
      el.classList.remove('transition_to_init', 'playable', 'paused')
      el.classList.add('playing')
    }, 
  
    OnBreakState: () => {
      console.log("add Classes On Break State")
      el.classList.remove('transition_to_init', 'playable', 'playing')
      el.classList.add('paused')
    }

  })
  
  /**
   * When player is ready
   */


  async function createYoutubePlayer(resolve, reject){
    controls = controls ? 1 : 0

    let options = {
      height: '390',
      width: '640',
      videoId: id,
      events: {
        'onReady': onState().onPlayerReady,
        'onStateChange': onPlayerStateChange
      },
      playerVars: {
          controls: controls,
          disablekb: 1,
          rel: 0,
          autohide: 1,
          autoplay: 1,
          playsinline: 1,
          cc_load_policy: 0
      },
    }


    // Add classes based on states
    function onPlayerStateChange(event) {
      if (event.data == YT.PlayerState.PLAYING) {
        addClasses().OnPlayState()
      } else if (event.data == YT.PlayerState.PAUSED) {
        addClasses().OnBreakState()
      }
    }

    // Create player
    function createPlayer(){
      player = new YT.Player(trgt, options);
      resolve(player)
    }

    // Check if Youtube Api is already loaded 
    let isApiLoaded = document.querySelector('[src*="youtube.com/iframe_api"]')

    if(!isApiLoaded){
      loadApi('https://www.youtube.com/iframe_api')
      window.onYouTubeIframeAPIReady = createPlayer
    } else {
      createPlayer()
    }

  }
  
  async function createVimeoPlayer(resolve, reject){

    controls = controls ? true : false
   
    let options = {
        id: id,
        width: 800,
        autoplay: true,
        byline: false,
        controls: controls,
        dnt: true,
        portrait: false,
        playsinline: true,
        title: false
      };

    // Create player
    player = new Vimeo.Player(trgt, options);
    
    // Return promise result
    player ? resolve(player) : reject()
    player.ready() ? onState().onPlayerReady() : reject()

    // Add classes based on states
    player.on('play', addClasses().OnPlayState)
    player.on('pause', addClasses().OnBreakState)

  }

  // Check If Autoplay was previously denied 
  // Allow  if not previously denied or allow if Previoulys denied but controls option are ON
  if( lve_autoplayDisable && !el.getAttribute('data-controls') ){
      onState().onFailure()
      return
  }

  new Promise((resolve, reject) => {
    if(platform == "youtube"){
      createYoutubePlayer(resolve, reject)
    } else if (platform == "vimeo"){
      createVimeoPlayer(resolve, reject)
    }
  }).catch(error => {
    // If Autoplay is prevented
    lve_autoplayDisable = true
    console.log(error)
    // Go direct to  page
    onState().onFailure()
  });  

}



/**
 * Ehance peertube link without loading heavy iframe
 * @param {string}    selector    DOM element to ehance
 * @param {function}  callback    function to call when player is created
 * @param {string}    target      Where to embded newly video player (Youtube specific)
 * @param {boolean}   denied      Disable video player creation when true (Denied multiplayer)
 */

function ehanceVideoLink(selector, callback, target, denied = () => { return false }){

  const replaceOnCLick = (video) => {
    video.addEventListener('click', (evt) => {
        if(!denied()){
          lve_abort = false
          video.setAttribute("data-href", video.getAttribute('href'))
          video.setAttribute("href", "javascript:void(0);")
          video.classList.add('transition_to_init')
          replaceByIframe(video, callback, target);
          return false;
        } else {
          console.log('Sorry, a video is already playing')
        }
      }, { once: true })
  }

  if(typeof selector == 'string'){

    let videos = document.querySelectorAll(selector)
    
    for (let video of videos) {
      video.setAttribute('role', 'button');

      // Prevent default no matter what
      video.addEventListener('click', (evt) => {
          evt.preventDefault();
      })

      // Replace by Iframe, Once
      replaceOnCLick(video)
    }

  } else {
    replaceOnCLick(selector)
  }
}
