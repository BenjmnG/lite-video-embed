let body = document.querySelector('body'),
    shareButton = document.querySelector('#share'),
    shareDialog = document.querySelector('#shareDialog'),
    mouseDown = document.querySelector('.mouseDown-container'),
    theater = null

    
/**
    Extend lve.js original fallback function with Spa integration 
*/

fallbackAutoplay = function(el){
    if(_spaLoad){
      const href = el.getAttribute('href')
      _spaLoad(href,true)
    } else {
      _fallbackAutoplay(el)
    }
}

let _fallbackAutoplay = fallbackAutoplay

/**
 *  This function filter list film by domain
 *  @param  { el }      Button clicked
*/
function sortDomain(el){

    let domaine = el.getAttribute('data-value'),
        films = document.querySelectorAll("#list li"),
        filmArray = Array.from(films),
        container = document.querySelector('#sortDomain');

    if( el.classList.contains('active')){
       el.classList.remove('active');
       container.classList.remove('active');
       
       let hided = document.querySelectorAll('#list li.hide')
        hided.forEach(e =>
            e.classList.remove('hide')
        )
    } else if( el.classList.contains('reset')){
       
       container.classList.remove('active');

       document.querySelector('#sortDomain button.active').classList.remove('active')
       
       let hided = document.querySelectorAll('#list li.hide')
        hided.forEach(e =>
            e.classList.remove('hide')
        )
    } else {

        let actived = document.querySelectorAll('#sortDomain .active'),
            hided = document.querySelectorAll('#list li.hide'),
            hide = document.querySelectorAll('#list li:not([data-domain="' + domaine + '"])')

        actived.forEach(e => e.classList.remove('active') )
        hided.forEach(e => e.classList.remove('hide') )
        hide.forEach(e => e.classList.add('hide') )
       
        el.classList.add('active')
        container.classList.add('active');

    }  
}



/**
 * This function group all utilities for display video in fullscreen
 * @param {player} the vimeo player
 */
const theaterMode = (player) => ({

    getParent: () => {
        // Update actual video container
        // Manage Vimeo (1) and youtube (2) container
        theater = player.element ? player.element : player.h
    },
    exit: () => {

        // Prevent loading player to trigger fallback 
        lve_abort = true

        theater.classList.remove('playing', 'played', 'playable', "paused")
        theater.classList.add('transition_to_exit')

        theaterMode(player).destroyPlayer(theater)

        // Reset data-video-embed behavior

        theaterMode(player).enableScroll();


        setTimeout(function(){
            body.classList.remove('playing',  'played',  'playable', "paused")
            theater.classList.remove('transition_to_exit')
            theater.setAttribute("href", theater.getAttribute('data-href'))
            //theaterMode(player).clickToPlay(theater);
        }, 250)
    },

    fallbackExit: (theater) => {
        // Make sure Player closed and Body abort theater view
        setTimeout(function(){
            if(theater.className.length > 0 ){
                theater.classList.remove('playing', 'played', 'playable')
            }
            if(body.className.length > 0 ){
                body.classList.remove('playing',  'played',  'playable')
            }
        }, 250)
    },

    destroyPlayer: (theater) => {
        theater.classList.add('clearing')
        setTimeout( () => {
            player.destroy().then( e =>  {
                theater.classList.remove('clearing', 'activated')
                theater.querySelector('.close').remove();
                //setTimeout( () => {
                    ehanceVideoLink(theater, theaterMode().forFilm, 'iframeContainer', theaterMode().deniedTheater)
                //},250)
            });
        },500)
    },

    onLoad: () => {
        body.classList.add('playable')
        // Enable theater mode on playing
        player.on('playing', function() {
            body.classList.remove('playable')
            body.classList.add('playing')
            theaterMode().clickToPause()
        });
        player.setLoop(true)
    },
    onPlay: () => {
        // Enable theater mode on playing
        player.on('play', function() {
            body.classList.remove('paused')
            body.classList.add('playing')

            theaterMode(player).clickToPause(theater)
        });
    },
    onBreak: () => {
        // Disable theater mode on pause
        player.on('pause', function() {

            theater.classList.add('transition_to_break')
            
            body.classList.remove('playing')
            body.classList.add('paused')

            setTimeout(function(){
                theater.classList.remove('transition_to_break')
            }, 750)

        })
    },
    clickToPlay : (el) => {
        el.addEventListener('click', () => {
            theater = player.element.parentNode
            theaterMode(player).onPlay()
            player.play()
        }, {once: true})
    },
    clickToPause : (el) => {
        (player.element).addEventListener('click', () => {
            player.pause()
            theaterMode(player).clickToPlay(el)
        }, {once: true})
    },
    disableScroll : () => {
    // Get the current page scroll position
        scrollTop = window.pageYOffset || document.documentElement.scrollTop;
        scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,

        // if any scroll is attempted, set this to the previous value
        window.onscroll = function() {
            window.scrollTo({
              top: scrollTop,
              left: scrollLeft,
              behavior: 'instant'
            });
        };
    },
    enableScroll : () => {
        window.onscroll = function() {};
    },

    enableExit: (onScroll, url = null) => {
        /* Allow exit possibilities*/    
        theaterMode().closeButton(url)
    
        player.on('playing', function() {
            if(onScroll){
                if ("ontouchstart" in document.documentElement){
                    theater.addEventListener('touchmove', 
                        e => {
                            if(theater.classList.contains("playing")){
                               theaterMode(player).exit()
                            }
                        }, 
                    {once: true})
                } else {
                    theater.addEventListener('wheel', 
                        e => {
                            if(theater.classList.contains("playing")){
                               theaterMode(player).exit()
                            }
                        }, 
                    {once: true})
                }
            }
        })
    },

    closeButton : (url) => {
      const tag = url ? 'a' : 'button';
      const attributes = url ? { href: url } : {};  // Object for attributes

      const svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
      svg.setAttribute('width', '25');
      svg.setAttribute('height', '25');
      svg.setAttribute('viewBox', '0 0 25 25');

      const line1 = document.createElementNS('http://www.w3.org/2000/svg', 'line');
      line1.setAttribute('x1', '0');
      line1.setAttribute('y1', '25');
      line1.setAttribute('x2', '25');
      line1.setAttribute('y2', '0');

      const line2 = document.createElementNS('http://www.w3.org/2000/svg', 'line');
      line2.setAttribute('x1', '0');
      line2.setAttribute('y1', '0');
      line2.setAttribute('x2', '25');
      line2.setAttribute('y2', '25');

      svg.appendChild(line1);
      svg.appendChild(line2);

      const element = document.createElement(tag);
      element.classList.add('close');
      element.appendChild(svg);

      if (url) {
        Object.assign(element, attributes);  // Assign attributes efficiently
      }

      element.addEventListener('click', () => {
        theaterMode(player).exit();
        theaterMode().fallbackExit(theater); // Attempt fallback
      });

      theater.parentNode.appendChild(element);

    },

    centerView: (el, t = 0) => {
        setTimeout(function(){
            if(body.classList == 'playing' || body.classList == 'playable'){
                el.scrollIntoView({block: "start"});
            }
        }, t) 
    },

    getCoord: (el, to) => {

        to = !to ? el : to

        let rect = el.getBoundingClientRect();

        to.style.setProperty('--tx', rect.x);
        to.style.setProperty('--ty', rect.y);
        to.style.setProperty('--th', rect.height);
        to.style.setProperty('--tw', rect.width);
    },

    increaseScreen: () => {

        theaterMode(player).getCoord(theater.parentNode)

        window.addEventListener("resize", (event) => {
            theaterMode(player).getCoord(theater.parentNode)
            theaterMode(player).centerView(theater)

        });
    },

    deniedTheater: () => {
        return body.classList.contains('playable') || body.classList.contains('playing') ? true : false
    },

    forFilm: (player) => {

        if(body.classList != 'playing'){

            theaterMode(player).getParent()
            theaterMode(player).onBreak()
            theaterMode(player).onLoad()
            theaterMode(player).increaseScreen()
            theaterMode(player).enableExit(false)

            var i = 0, 
            interval = setInterval(function() {
                theaterMode(player).centerView(theater, 0)
                i++;
                if(i >= 10) clearInterval(interval); // stop it
            }, 50);
            theaterMode(player).disableScroll()

            player.on('playing', function() {

                window.addEventListener("resize", (event) => {
                    theaterMode(player).centerView(theater, 500)
                })

               screen.orientation.addEventListener("change", function(e) {
                    theaterMode(player).centerView(theater, 500)
                })
            })
        }
    },

    forDoc: (player) => {
        window.scrollTo(0,0)
        theaterMode(player).getParent()
        theaterMode(player).onLoad()
        theaterMode(player).onBreak()
        theaterMode(player).increaseScreen()
        theaterMode(player).enableExit(false)

        player.on('playing', function() {
            theaterMode(player).onPlay()
        })

        setTimeout(() => {
            document.querySelector('[data-video-embed]').scrollIntoView({ block: "center" });
        }, "400")
    },

    forIndex: (player) => {
        theaterMode(player).getParent()
        player.on('playing', function() {
            //theater.classList.remove('playable')
            //theater.classList.add('playing')
            body.classList.add('playing')
            theaterMode().closeButton('/films/')
            theaterMode(player).enableExit(false, '/films/')
        });

        player.on('ended', function() {
            if(_spaLoad){
              _spaLoad('/films/',true)
            } else {
              window.location.replace('/films/')
            }
        });

    }
})

/* This function unobfuscate email and phone number on DOM */
function obf(){

    // use onlinetools.com/unicode/escape-unicode with \u00%h to get inverse
    function unicodeToChar(text) {
       return text.replace(/\\u[\dA-F]{4}/gi, 
            function (match) {
                return String.fromCharCode(parseInt(match.replace(/\\u/g, ''), 16));
            });
    }

    (document.querySelectorAll('.obf em')).forEach(e => {
        let v = e.getAttribute('data-value')
        e.innerHTML = unicodeToChar(v)
    })
}



/** 
 * This  group page event in a main function.
 * Page Event function are call on page load 
 * or by miniSPA library when loading page with transition
*/
const event = () => ({

  index: () => {
    let main       = document.querySelector('[data-page="index"]')
    let dve   = document.querySelector('[data-page="index"] [data-video-embed]')

    // Prevent default no matter what
    dve.addEventListener('click', (evt) => {
        evt.preventDefault();
    })
    // Replace by Iframe, Once
    dve.addEventListener('click', (evt) => {
        dve.classList.add('playable')
        replaceByIframe(dve, theaterMode().forIndex)
    }, {once: true})


    // if autoplay fail, replace video with a fallback image
    let video = dve.querySelector('video')
    let promise = video.play();

    if (promise !== undefined) {
      promise.then(_ => {
        // Autoplay started!
      }).catch(error => {
        // Autoplay not allowed!
        // Mute video and try to play again
        //video.muted = true;
        //video.play();
        var img = document.createElement('img');
        img.src = '/media/introsite-ios_fallback.jpg';
        img.classList.add('ios_fallback')
        dve.appendChild(img);
      });
    }

  },
  filmsList: () => {
    ehanceVideoLink('[data-video-embed]',  theaterMode().forFilm, 'iframeContainer', theaterMode().deniedTheater)
  },
  film: () => {
    ehanceVideoLink('[data-video-embed]', )
  },
  doc: () => {
    ehanceVideoLink('[data-video-embed]', theaterMode().forDoc, )
  },
  info: () => {
    obf()
  },
  reel: () => {
    ehanceVideoLink('[data-video-embed]', )
  }

})



/* On init */
body.classList.remove('noJs')
console.log("%cDesign + Code: \nhttps://bnjm.eu", "font-family: monospace; font-size: 1.5em;")

